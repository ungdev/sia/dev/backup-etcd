# backup-etcd

Service pour générer les fichier de backup ETCD de OKD


## Fonctionnement général

Après avoir supprimé les précédentes versions de backup ETCD sur le conteneur, ce dernier se connectera en ssh sur un master node du cluster OKD. Il lancera ensuite la commande pour générer les fichiers de backup ETCD, puis les copiera sur le conteneur.


## Nécessaire pour le fonctionnement

- Copier le fichier .env.example en .env et y faire les modifications nécessaires.
- Fournir le fichier contenant la clé privée du master node du cluster OKD en tant que Secret avec une clé nommée "okd".
- Faire une backup des volumes afin de pouvoir récupérer les fichiers de backup.


## Note : Récupération des fichiers de backup
Afin de récupérer les fichiers de backup, il est prévu d'utiliser un service de backup qui s'occupera de la backup des volumes du conteneur, et récupérera et stockera ainsi les backup ETCD.


#!/bin/bash

# Il faut pour ce script un fichier contenant la clé de l'utilisateur core du master dont la PATH devra se trouver dans la variable `password`
password=./.ssh/okd

ssh_cmd="ssh -q -i $password -o BatchMode=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $USER@$SERVER"

# On supprime les fichiers de l'ancienne backup sur le conteneur est sur le node master okd
echo "Suppression backup précédente en local"
rm -f $VOLUME/snapshot*
rm -f $VOLUME/static*
echo "Suppression backup précédente sur le master node okd"
$ssh_cmd sudo rm -r backup-etcd

# On lance la commande générant les fichiers de backup, et les place dans un dossier nommé "backup-etcd"
echo "Génération des fichiers de backups"
$ssh_cmd sudo /usr/local/bin/cluster-backup.sh /var/home/core/backup-etcd

# On donne l'ownership à core pour les fichiers de backup et le dossier qui les contient
echo "Attribution des permissions des fichiers de backup à" $USER
$ssh_cmd sudo chown -R $USER:$USER /var/home/core/backup-etcd

# On copie les fichiers de backup ETCD sur le Pod
echo "Copie des fichiers de backup en local"
scp -q -i $password -o BatchMode=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $USER@$SERVER:/var/home/core/backup-etcd/snapshot* $VOLUME
scp -q -i $password -o BatchMode=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $USER@$SERVER:/var/home/core/backup-etcd/static* $VOLUME

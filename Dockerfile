# Dockerfile

FROM ubuntu:18.04 

RUN apt update -y && apt upgrade -y
RUN apt install -y openssh-client curl

WORKDIR /srv

RUN mkdir ./.ssh
RUN chmod 755 ./.ssh



COPY ./script_backup_etcd.sh ./script_backup_etcd.sh
RUN chmod 755 ./script_backup_etcd.sh

RUN mkdir ./etcd

VOLUME /srv/etcd
RUN chmod o+w /srv/etcd


CMD ./script_backup_etcd.sh
